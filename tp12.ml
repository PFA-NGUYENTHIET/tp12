open Graphics

let dim = 10 
let coeff = 100

(* Question 1 *)

let random_bool_matrix () =
    Array.init dim ( fun i -> 
        Array.init dim ( fun j ->
            Random.bool ()
        )
    )

let draw_pixel i j c =
    set_color c;
    fill_rect (i*coeff) (j*coeff) coeff coeff

(* Question 2 *)

let rec matrix_iter i j n f m =
    if i<n then
        if j>=n then
            matrix_iter (i+1) 0 n f m
        else
            f i j m;
            matrix_iter i (j+1) n f m

let draw_BW_image m =
    matrix_iter 0 0 (Array.length(m)) (fun i j m ->
        if m.(i).(j) then
            draw_pixel i j black
        else
            draw_pixel i j white 
    ) m

(* Question 3 *)
type uf = int array

let create n =
    Array.init n (fun i -> i)

let find uf x =
    uf.(x)

let union uf x y =
    let rec aux uf x =
        let p = find uf x in
        if ( p = x ) then
            x
        else
            find uf p 
    in

    uf.(aux uf x) <- aux uf y;
    Array.iteri (fun i x -> 
        uf.(i) <- aux uf i
    ) uf

let print_array a =
    Array.iter (fun x -> print_int x; Printf.printf " ") a;
    Printf.printf "\n"

(* Question 4 *)
let int_of_pixel (i,j) =
    i*dim + j

(* Question 5 *)
let calcul_parties_connexes uf m =
    matrix_iter 0 0 dim (fun i j m ->
        if i < 9 then
            if m.(i).(j) = m.(i+1).(j) then
                union uf (int_of_pixel (i,j)) (int_of_pixel(i+1,j));

        if j < 9 then
            if m.(i).(j) = m.(i).(j+1) then
                union uf (int_of_pixel (i,j)) (int_of_pixel(i,j+1));
    ) m
let inc_color = 83 

let new_color = 
    let r = ref 10 in
    let g = ref 10 in
    let b = ref 10 in
    fun () ->
        let b' = !b + inc_color in
        let g' = !g + (b'/255*inc_color) in
        let r' = !r + (g'/255*inc_color) in
        b := b' mod 256 ;
        g := g' mod 256 ;
        r := r' mod 256 ;
        Graphics.rgb !r !g !b

let draw_COLOR_image uf image =
    let ht = Hashtbl.create (dim * dim) in
    Array.iter (fun x ->
        try Hashtbl.find ht x; ()
        with Not_found ->
            Hashtbl.add ht x (new_color ())
        ) uf;

    matrix_iter 0 0 dim (fun i j m -> 
        (* draw_pixel i j (Hashtbl.find ht (int_of_pixel (i,j))) *)
        print_int i;
        print_int j;
        Printf.printf " ";
        draw_pixel i j black
     ) image

let () =
    Random.self_init ();
    let len = string_of_int (dim*coeff) in
    open_graph (" " ^ len ^ "x" ^ len);

    let image = random_bool_matrix () in
    let uf = create ( dim * dim ) in
    calcul_parties_connexes uf image;
    draw_BW_image image
    (* draw_COLOR_image uf image; *)
    (* ignore (read_key ()) *)
