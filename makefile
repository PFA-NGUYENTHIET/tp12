.PRECIOUS: %.cmi %.cmo

all : tp12.ml
	ocamlc -o a.out graphics.cma tp12.ml

clean :
	rm -rf *.cmi
	rm -rf *.cmo
	rm -rf a.out

# compile :
# 	ocamlbuild -use-ocamlfind -package graphics tp12.byte

# clean:
# 	ocamlbuild -clean

